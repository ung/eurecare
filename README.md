# eurecare



## Description

EURECARE is a website and mobile app created by Lou Marze, Elena Lasalle,Lina Chiadmi and Guillaume Ung providing informations regarding disabled people on who to contact in the school, how to access the building with reduced mobilty and other things.


## Ideas to implement

- Anonymous group chat with UNIX login identification, chat moderation and reporting system
- Carpooling (free!) to help injured/disabled people go to school

